#

# see also [*](https://pandoc.org/MANUAL.html)
export PATH=../bin:$PATH

dir=${1:-.}
ls -1a $dir | grep -v index\. > $dir/ls.txt
qm=$(ipfs add -Q -r $dir)
cd $dir
pwd=$(pwd -P)
pwd=${pwd#$HOME/}
date=$(date +%D)
tic=$(date +%s)
ip=$(curl -s https://dyn℠.ml/cgi-bin/remote_addr.pl)
cat >index.yml <<EOF
---
title: "index of $pwd"
date: $date
dir: $dir
tic: $tic
pwd: $pwd
ip: $ip
qm: $qm
GW: https://gateway.ipfs.io
...
EOF
sed -e 's/^\(.*\)/ - [\1](\1)/' ls.txt > ls.md

pandoc -t html -o index.html --template=index.tmpl --metadata-file index.yml ls.md

true; # $Source: /my/shell/script/mkindex.sh$
