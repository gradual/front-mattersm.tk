#

export PATH=../bin:$PATH
hostfile=${1:-default.yml}
host=${hostfile##*/}
host=${host%.yml}
if [ -e $hostfile ]; then
matter=$hostfile
else
matter=../matters/$host.yml
fi
if [ ! -e $matter ]; then
git add $matter
sed -e "s/domain: .*/domain: {{$host}}/" ../matters/default.yml > $matter
vi $matter
fi
domain=$(cat $matter | xyml domain)

if test -e index.md ; then
sed -i -e "s/domain: .*/domain: $domain/" \
    -e "s, - \"../matters/.*, - \"$matter\"," index.md
else 
cat <<EOF > index.md
---
host: $host
moustache:
 - "$matter"
---
## {{title}}

Our {{name}} website is on the [ipms] network
and therefore is hostless, in order to view it you
have to run a local node

more info at <http://{{domain}}/install>

for any question we are reachable at [{{email}}](mailto:{{email}})

Thanks<br>
{{RP}}<br>
--&nbsp;<br>
{{email}}

[IPMS]: {{SEARCH}}=IPMS+%22blockRing™%22
EOF
fi
perl -S moustache.pl index.md public/index.md
perl -S moustache.pl 301.htm public/301.html

pandoc -t html -f markdown -o public/index.html public/index.md
surge $PWD/public $host
echo url: http://www.$domain

true; # $Source: /my/shell/scripts/makesite.sh$
