---
domain: {{domain}}
---
# How to build a quick site

1. fill up the front-matter form
2. run sh make.sh {{domain}}
3. voila the result is at http://www.{{domain}}
